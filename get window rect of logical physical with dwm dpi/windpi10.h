#ifndef LoadLibrary
#error "You must include Windows.h before include dywa.h"
#endif
#ifndef WINDPI10_H
#define WINDPI10_H

typedef struct windpi10_ERR {
	PCSTR pszW32API;
	HRESULT hr;
} windpi10_ERR;

BOOL DpiGetPhyWndRect(windpi10_ERR *pErr, HWND hWnd, RECT *prcWnd);

BOOL DpiGetLogWndRect(windpi10_ERR *pErr, HWND hWnd, RECT *prcWnd);

#endif/* WINDPI10_H */
