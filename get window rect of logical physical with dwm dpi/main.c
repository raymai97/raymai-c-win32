#include <stdio.h>
#define WIN32_LEAN_AND_MEAN
#include <Windows.h>
#include <WTypes.h>
#include "dywa.h"
#include "windpi10.h"

static void MyPrintRect(char const *pszName, RECT const *prc)
{
	int x1 = prc->left; int y1 = prc->top;
	int x2 = prc->right; int y2 = prc->bottom;
	int cx = x2 - x1; int cy = y2 - y1;
	printf("%s = %d x %d aka { %d, %d, %d, %d }\n",
		pszName, cx, cy, x1, y1, x2, y2);
}

int main(void)
{
	struct {
		char const *pszW32API;
		HRESULT hr;
	} err = { 0 };
	windpi10_ERR windpi10_err = { 0 };

	HWND hwnd = 0;	
	RECT rc = { 0 };

	printf("Taking foreground active window as target after 2 sec...\n");
	Sleep(2000);
	hwnd = GetForegroundWindow();
	printf("Target HWND = %.8lX\n", (DWORD)hwnd);
	
	if (!DpiGetLogWndRect(&windpi10_err, hwnd, &rc))
	{
		err.pszW32API = windpi10_err.pszW32API;
		err.hr = windpi10_err.hr;
		goto eof;
	}
	MyPrintRect("LogWndRect", &rc);
	if (!DpiGetPhyWndRect(&windpi10_err, hwnd, &rc))
	{
		err.pszW32API = windpi10_err.pszW32API;
		err.hr = windpi10_err.hr;
		goto eof;
	}
	MyPrintRect("PhyWndRect", &rc);
	if (DYWApfnSetProcessDPIAware)
	{
		HRESULT hr = DywaSetProcessDPIAware();
		{
			printf("SetProcessDPIAware() return 0x%.8lX\n", hr);
		}
		if (!DpiGetLogWndRect(&windpi10_err, hwnd, &rc))
		{
			err.pszW32API = windpi10_err.pszW32API;
			err.hr = windpi10_err.hr;
			goto eof;
		}
		MyPrintRect("LogWndRect", &rc);
		if (!DpiGetPhyWndRect(&windpi10_err, hwnd, &rc))
		{
			err.pszW32API = windpi10_err.pszW32API;
			err.hr = windpi10_err.hr;
			goto eof;
		}
		MyPrintRect("PhyWndRect", &rc);
	}
eof:
	if (FAILED(err.hr))
	{
		printf("Error: %s() return 0x%.8lX\n", err.pszW32API, err.hr);
	}
	return 0;
}
