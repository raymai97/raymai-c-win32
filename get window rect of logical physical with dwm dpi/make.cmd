@echo off
call :cleanup

REM Set CFLAGS to "-DUNICODE" for UNICODE build.
cl -c -MD %CFLAGS% ^
    dywa.c ^
    windpi10.c ^
    main.c || goto gg

link ^
    kernel32.lib ^
    user32.lib ^
    version.lib ^
    dywa.obj ^
    windpi10.obj ^
    main.obj ^
    -out:GetWindowRect_Logical_Physical_DWM_DPI.exe ^
    -subsystem:console ^
    -fixed:no || goto gg

call :cleanup
exit/b

:gg
echo.
echo. ERROR
echo.
pause
exit/b

:cleanup
if exist dywa.obj del dywa.obj
if exist windpi10.obj del windpi10.obj
if exist main.obj del main.obj
exit/b
