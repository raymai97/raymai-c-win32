#define WIN32_LEAN_AND_MEAN
#include <Windows.h>
#include <WTypes.h>
#include "windpi10.h"
#include "dywa.h"

#define MYfnnGetWindowRect  "GetWindowRect"

#undef DWMWA_EXTENDED_FRAME_BOUNDS
#define DWMWA_EXTENDED_FRAME_BOUNDS  (9)

BOOL DpiGetPhyWndRect(windpi10_ERR *pErr, HWND hWnd, RECT *prcWnd)
{
	HRESULT hr = 0;
	if (DYWApfnDwmIsCompositionEnabled && !DYWApfnLogToPhyPointForPmDpi)
	{
		BOOL isDwmEnabled = FALSE;
		hr = DywaDwmIsCompositionEnabled(&isDwmEnabled);
		if (FAILED(hr))
		{
			pErr->pszW32API = DYWAfnnDwmIsCompositionEnabled;
			goto eof;
		}
		if (isDwmEnabled)
		{
			hr = DywaDwmGetWindowAttribute(hWnd, DWMWA_EXTENDED_FRAME_BOUNDS, prcWnd, sizeof(*prcWnd));
			if (FAILED(hr))
			{
				pErr->pszW32API = DYWAfnnDwmGetWindowAttribute;
			}
			goto eof;
		}
	}
	if (GetWindowRect(hWnd, prcWnd) == FALSE)
	{
		hr = HRESULT_FROM_WIN32(GetLastError());
		pErr->pszW32API = MYfnnGetWindowRect;
		goto eof;
	}
eof:
	pErr->hr = hr;
	return SUCCEEDED(hr);
}

BOOL DpiGetLogWndRect(windpi10_ERR *pErr, HWND hWnd, RECT *prcWnd)
{
	HRESULT hr = 0;
	if (GetWindowRect(hWnd, prcWnd) == FALSE)
	{
		hr = HRESULT_FROM_WIN32(GetLastError());
		pErr->pszW32API = MYfnnGetWindowRect;
		goto eof;
	}
	if (DYWApfnPhyToLogPointForPmDpi)
	{
		POINT *pPt = (POINT*)prcWnd;
		hr = DywaPhyToLogPointForPmDPI(hWnd, &pPt[0]);
		if (FAILED(hr))
		{
			pErr->pszW32API = DYWAfnnPhyToLogPointForPmDpi;
			goto eof;
		}
		hr = DywaPhyToLogPointForPmDPI(hWnd, &pPt[1]);
		if (FAILED(hr))
		{
			pErr->pszW32API = DYWAfnnPhyToLogPointForPmDpi;
			goto eof;
		}
	}
eof:
	pErr->hr = hr;
	return SUCCEEDED(hr);
}
