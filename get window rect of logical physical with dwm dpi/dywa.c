#define WIN32_LEAN_AND_MEAN
#include <Windows.h>
#include <WTypes.h>
#include "dywa.h"

HRESULT DywaSetProcessDPIAware(void)
{
	typedef BOOL(WINAPI *fn_t)(void);
	fn_t fn = (fn_t)DYWApfnSetProcessDPIAware;
	if (fn) return fn() ? S_OK : E_FAIL;
	return E_NOTIMPL;
}

HRESULT DywaDwmGetWindowAttribute(HWND hWnd, DWORD dwAttr, void *pAttr, DWORD cbAttr)
{
	typedef HRESULT(WINAPI *fn_t)(HWND, DWORD, void*, DWORD);
	fn_t fn = (fn_t)DYWApfnDwmGetWindowAttribute;
	return fn ? fn(hWnd, dwAttr, pAttr, cbAttr) : E_NOTIMPL;
}

HRESULT DywaDwmIsCompositionEnabled(BOOL *pVal)
{
	typedef HRESULT(WINAPI *fn_t)(BOOL*);
	fn_t fn = (fn_t)DYWApfnDwmIsCompositionEnabled;
	return fn ? fn(pVal) : E_NOTIMPL;
}

HRESULT DywaPhyToLogPointForPmDPI(HWND hWnd, POINT *pPt)
{
	typedef BOOL(WINAPI *fn_t)(HWND, POINT*);
	fn_t fn = (fn_t)DYWApfnPhyToLogPointForPmDpi;
	return fn ? fn(hWnd, pPt) : E_NOTIMPL;
}

HRESULT DywaLogToPhyPointForPmDPI(HWND hWnd, POINT *pPt)
{
	typedef BOOL(WINAPI *fn_t)(HWND, POINT*);
	fn_t fn = (fn_t)DYWApfnLogToPhyPointForPmDpi;
	return fn ? fn(hWnd, pPt) : E_NOTIMPL;
}
