#ifndef LoadLibrary
#error "You must include Windows.h before include dywa.h"
#endif
#ifndef RAYMAI97_DYWA_H
#define RAYMAI97_DYWA_H

#define DYWAhMod_dwmapi  LoadLibrary(TEXT("dwmapi"))
#define DYWAhMod_user32  LoadLibrary(TEXT("user32"))

#define DYWAfnnSetProcessDPIAware  "SetProcessDPIAware"
#define DYWApfnSetProcessDPIAware  GetProcAddress(DYWAhMod_user32, DYWAfnnSetProcessDPIAware)
HRESULT DywaSetProcessDPIAware(void);

#define DYWAfnnDwmGetWindowAttribute  "DwmGetWindowAttribute"
#define DYWApfnDwmGetWindowAttribute  GetProcAddress(DYWAhMod_dwmapi, DYWAfnnDwmGetWindowAttribute)
HRESULT DywaDwmGetWindowAttribute(HWND hWnd, DWORD dwAttr, void *pAttr, DWORD cbAttr);

#define DYWAfnnDwmIsCompositionEnabled  "DwmIsCompositionEnabled"
#define DYWApfnDwmIsCompositionEnabled  GetProcAddress(DYWAhMod_dwmapi, DYWAfnnDwmIsCompositionEnabled)
HRESULT DywaDwmIsCompositionEnabled(BOOL *pVal);

#define DYWAfnnPhyToLogPointForPmDpi  "PhysicalToLogicalPointForPerMonitorDPI"
#define DYWApfnPhyToLogPointForPmDpi  GetProcAddress(DYWAhMod_user32, DYWAfnnPhyToLogPointForPmDpi)
HRESULT DywaPhyToLogPointForPmDPI(HWND hWnd, POINT *pPt);

#define DYWAfnnLogToPhyPointForPmDpi  "LogicalToPhysicalPointForPerMonitorDPI"
#define DYWApfnLogToPhyPointForPmDpi  GetProcAddress(DYWAhMod_user32, DYWAfnnLogToPhyPointForPmDpi)
HRESULT DywaLogToPhyPointForPmDPI(HWND hWnd, POINT *pPt);

#endif/* RAYMAI97_DYWA_H */
