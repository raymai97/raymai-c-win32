#include <Windows.h>
#include "Boon_GetMonitorRect.h"

static LPCTSTR const MyWndClassName = TEXT("Boon_GetMonitorRect");

static void GoGetMonRect(HWND hWnd)
{
    LPCTSTR pszRoutine = TEXT("fallback");
    TCHAR szText[1024] = { 0 }, *pszText = szText;
    BOOL isErr = FALSE;
    RECT rcMonitor = { 0 };
    RECT rcWork = { 0 };
    LONG x = 0, y = 0, cx = 0, cy = 0;
    switch (Boon_GetMonitorRect(hWnd, &rcMonitor, &rcWork))
    {
    case 1:
        pszRoutine = TEXT("GetMonitorInfo()");
        break;
    case 0:
        break;
    default:
        isErr = TRUE;
        pszText += wsprintf(pszText, TEXT("Win32 error: %ld"), GetLastError());
        goto eof;
    }
    pszText += wsprintf(pszText, TEXT("Using %s routine.\n\n"), pszRoutine);
    x = rcMonitor.left;
    y = rcMonitor.top;
    cx = rcMonitor.right - x;
    cy = rcMonitor.bottom - y;
    pszText += wsprintf(pszText,
        TEXT("[Monitor] pos (%ld, %ld) size (%ld, %ld)\n\n"),
        x, y, cx, cy);
    x = rcWork.left;
    y = rcWork.top;
    cx = rcWork.right - x;
    cy = rcWork.bottom - y;
    pszText += wsprintf(pszText,
        TEXT("[WorkArea] pos (%ld, %ld) size (%ld, %ld)"),
        x, y, cx, cy);
eof:
    MessageBox(hWnd, szText, MyWndClassName,
        isErr ? MB_ICONHAND : MB_ICONASTERISK);
}

static LRESULT CALLBACK MyWndProc(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
    LRESULT lResult = 0;
    BOOL fOverrid = 0;
    if (msg == WM_DESTROY)
    {
        PostQuitMessage(0);
    }
    else if (msg == WM_ERASEBKGND)
    {
        HDC hdcDst = (HDC)wParam;
        RECT rc;
        GetClientRect(hWnd, &rc);
        FillRect(hdcDst, &rc, (HBRUSH)(COLOR_3DFACE + 1));
        OffsetRect(&rc, 20, 20);
        SetBkMode(hdcDst, TRANSPARENT);
        SetTextColor(hdcDst, GetSysColor(COLOR_BTNTEXT));
        DrawText(hdcDst, TEXT("Please double-click or press ENTER."), -1, &rc, 0);
    }
    else if (msg == WM_KEYDOWN)
    {
        WORD vk = LOWORD(wParam);
        if (vk == VK_RETURN)
        {
            GoGetMonRect(hWnd);
        }
    }
    else if (msg == WM_LBUTTONDBLCLK)
    {
        GoGetMonRect(hWnd);
    }
    if (!fOverrid)
    {
        lResult = DefWindowProc(hWnd, msg, wParam, lParam);
    }
    return lResult;
}

int AppMain(void)
{
    WNDCLASS wc = { 0 };
    HWND hWnd = 0;
    MSG msg = { 0 };
    wc.hCursor = LoadCursor(0, IDC_ARROW);
    wc.lpszClassName = MyWndClassName;
    wc.lpfnWndProc = MyWndProc;
    wc.style = CS_DBLCLKS;
    RegisterClass(&wc);
    hWnd = CreateWindowEx(0,
        MyWndClassName, MyWndClassName,
        WS_OVERLAPPEDWINDOW,
        // x, y, width, height
        CW_USEDEFAULT, CW_USEDEFAULT, 500, 300,
        // hWndParent, hMenu, hInst, lpParam
        0, 0, 0, 0);
    ShowWindow(hWnd, SHOW_OPENWINDOW);
    while (GetMessage(&msg, 0, 0, 0))
    {
        TranslateMessage(&msg);
        DispatchMessage(&msg);
    }
    return (int)(msg.wParam);
}

void RawMain(void)
{
    ExitProcess(AppMain());
}
