#include "Boon_GetMonitorRect.h"

EXTERN_C int
Boon_GetMonitorRect(
    HWND hWnd,
    RECT *prcMonitor,
    RECT *prcWork)
{
    typedef BOOL(WINAPI *fnGetMonitorInfo_t)(HMONITOR, MONITORINFO *);
    typedef HMONITOR(WINAPI *fnMonitorFromWindow_t)(HWND, DWORD);
    int ret = -1;
    HANDLE hMod_user32 = 0;
    fnGetMonitorInfo_t pfnGetMonitorInfo = 0;
    fnMonitorFromWindow_t pfnMonitorFromWindow = 0;
    hMod_user32 = GetModuleHandle(TEXT("user32"));
    if (!hMod_user32) goto eof;
    pfnGetMonitorInfo = (fnGetMonitorInfo_t)GetProcAddress(hMod_user32,
#ifdef UNICODE
        "GetMonitorInfoW"
#else
        "GetMonitorInfoA"
#endif
    );
    pfnMonitorFromWindow = (fnMonitorFromWindow_t)GetProcAddress(hMod_user32,
        "MonitorFromWindow");
    if (hWnd && pfnGetMonitorInfo && pfnMonitorFromWindow)
    {
        HMONITOR hMon = pfnMonitorFromWindow(hWnd, MONITOR_DEFAULTTOPRIMARY);
        if (hMon)
        {
            MONITORINFO mi = { sizeof(mi) };
            if (pfnGetMonitorInfo(hMon, &mi))
            {
                if (prcMonitor)
                {
                    *prcMonitor = mi.rcMonitor;
                }
                if (prcWork)
                {
                    *prcWork = mi.rcWork;
                }
                ret = 1;
                goto eof;
            }
        }
    }
    if (prcMonitor)
    {
        int cx = GetSystemMetrics(SM_CXSCREEN);
        int cy = GetSystemMetrics(SM_CYSCREEN);
        if (cx && cy)
        {
            SetRect(prcMonitor, 0, 0, cx, cy);
            ret = 0;
        }
        else goto eof;
    }
    if (prcWork)
    {
        if (SystemParametersInfo(SPI_GETWORKAREA, 0, prcWork, 0))
        {
            ret = 0;
        }
    }
eof:
    return ret;
}
