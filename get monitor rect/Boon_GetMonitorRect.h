#pragma once
#include <Windows.h>

// Parameters:
// [in][opt] hWnd
// [out][opt] prcMonitor
// [out][opt] prcWork
//
// "hWnd" is used to determine which monitor to retrieve RECT from.
// If pass NULL, it assumes primary monitor.
//
// If return 1, func success by GetMonitorInfo() routine.
// If return 0, func success by fallback routine.
// Otherwise, check GetLastError() for Win32 error.
//
EXTERN_C int
Boon_GetMonitorRect(
    HWND hWnd,
    RECT *prcMonitor,
    RECT *prcWork);
