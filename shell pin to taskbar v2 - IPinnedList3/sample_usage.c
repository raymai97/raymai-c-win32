#include "IPinnedList3.h"

#define PLMC_EXPLORER  (4)

enum Usage {
	Usage_Pin,
	Usage_Unpin
};

// Since Win7, we can pin to taskbar is by invoking "Pin to Taskbar" verb.
// Since Win10 1809 (or 1803?) it will just return error 0x80070057.
// We need to use IPinnedList3::Modify() instead.

// Precondition:
// - CoInitialize(0);
HRESULT Test(int usage)
{
    WCHAR const *pszPath = L"C:\\Windows\\regedit.exe";
    void *pidlPath = 0;
    HRESULT hr = 0;
    IPinnedList3 *pPL3 = 0;

    pidlPath = ILCreateFromPathW(pszPath);
    if (!pidlPath) goto eof;

    hr = CoCreate_IPinnedList3(&pPL3);
    if (FAILED(hr)) goto eof;

    if (usage == Usage_Pin)
	{
        hr = pPL3->lpVtbl->Modify(pPL3, 0, pidlPath, PLMC_EXPLORER);
	}
	if (usage == Usage_Unpin)
	{
        hr = pPL3->lpVtbl->Modify(pPL3, pidlPath, 0, PLMC_EXPLORER);
	}
eof:
    if (pPL3)
    {
        pPL3->lpVtbl->Release(pPL3);
    }
    if (pidlPath)
    {
        ILFree(pidlPath);
    }
    return hr;
}
