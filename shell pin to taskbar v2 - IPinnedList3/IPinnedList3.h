#pragma once
#include <Windows.h>

typedef struct IPinnedList3 IPinnedList3;
typedef struct IPinnedList3Vtbl IPinnedList3Vtbl;

struct IPinnedList3
{
	struct IPinnedList3Vtbl *lpVtbl;
};

struct IPinnedList3Vtbl
{
	void *pfnQueryInterface;
	void *pfnAddRef;
	ULONG(__stdcall *Release)(IPinnedList3 *pThis);
	void *pfn_unused__[13];
	HRESULT(__stdcall *Modify)(IPinnedList3 *pThis, void *pidlToUnpin, void *pidlToPin, int plmc);
};

HRESULT CoCreate_IPinnedList3(IPinnedList3 **pp);
