#include "IPinnedList3.h"
#include <objbase.h>

static const GUID CLSID_TaskbandPin =
{
	0x90aa3a4e, 0x1cba, 0x4233,
	{ 0xb8, 0xbb, 0x53, 0x57, 0x73, 0xd4, 0x84, 0x49}
};

static const GUID IID_IPinnedList3 =
{
	0x0dd79ae2, 0xd156, 0x45d4,
	{ 0x9e, 0xeb, 0x3b, 0x54, 0x97, 0x69, 0xe9, 0x40 }
};

HRESULT CoCreate_IPinnedList3(IPinnedList3 **pp)
{
	return CoCreateInstance(&CLSID_TaskbandPin, 0, CLSCTX_INPROC_SERVER, &IID_IPinnedList3, (void **)pp);
}
