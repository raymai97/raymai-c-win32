#include "Boon_EnableBlurBehind.h"

typedef struct ACCENTPOLICY
{
    DWORD state;
    DWORD flags;
    DWORD color;
    DWORD animId;
} ACCENTPOLICY;

typedef struct WINCOMPATTRDATA
{
    int attr;
    PVOID pData;
    ULONG cbData;
} WINCOMPATTRDATA;

BOOL
Boon_EnableBlurBehind(
    HWND hWnd,
    BOOL fEnable)
{
    HMODULE hMod = GetModuleHandleA("user32");
    if (hMod)
    {
        typedef BOOL(WINAPI* fn_t)(HWND, WINCOMPATTRDATA*);
        fn_t fn = (fn_t)GetProcAddress(hMod, "SetWindowCompositionAttribute");
        if (fn)
        {
            ACCENTPOLICY ac = {0};
            WINCOMPATTRDATA data = {0};
            if (fEnable)
            {
                ac.state = 3; // ACCENT_ENABLE_BLURBEHIND
            }
            data.attr = 19; // WCA_ACCENT_POLICY
            data.pData = &ac;
            data.cbData = sizeof(ac);
            return fn(hWnd, &data);
        }
    }
    return FALSE;
}
