#include <Windows.h>
#include "Boon_EnableBlurBehind.h"
#include "boon_layeredwindow.h"

static LPCTSTR const MyWndClassName = TEXT("Boon_LayeredWindow");

static DWORD argb(DWORD a, DWORD r, DWORD g, DWORD b)
{
    DWORD pixel = 0;
    pixel |= (BYTE)(a); pixel <<= 8;
    pixel |= (BYTE)(r); pixel <<= 8;
    pixel |= (BYTE)(g); pixel <<= 8;
    pixel |= (BYTE)(b);
    return pixel;
}

static void My32bppTextOut(
    HDC hdc,
    DWORD *pBmpPixels,
    LONG cxBmp,
    LONG cyBmp,
    LONG off_x,
    LONG off_y,
    LPCTSTR lpszText,
    int cchText)
{
    RECT rc;
    SetRect(&rc, off_x, off_y, off_x, off_y);
    if (cchText < 0) { cchText = lstrlen(lpszText); }
    DrawText(hdc, lpszText, cchText, &rc, DT_CALCRECT);
    DrawText(hdc, lpszText, cchText, &rc, 0);
    // Post-process every pixel touched by GDI DrawText()
    {
        LONG x, max_x = min(rc.right, cxBmp);
        LONG y, max_y = min(rc.bottom, cyBmp);
        for (y = rc.top; y < max_y; ++y)
        {
            for (x = rc.left; x < max_x; ++x)
            {
                // Easy way: Make opaque
                pBmpPixels[y * cxBmp + x] |= 0xFF000000;
            }
        }
    }
}

static BOOL CALLBACK MyRenderProc(
    HDC hdcBmp,
    DWORD *pBmpPixels,
    LONG cxBmp,
    LONG cyBmp,
    void *pUser)
{
    LONG x, y;
    HFONT hFont = 0;
    HGDIOBJ old_hFont = 0;
    (void)(pUser);
    for (y = 0; y < cyBmp; ++y)
    {
        DWORD a, r, r2, g, g2, b, b2;
        r = 0;
        g = 0;
        b = (y * 255) / cyBmp;
        for (x = 0; x < cxBmp; ++x)
        {
            a = 255 - ((x * 128) / cxBmp);
            // premultiplied ARGB
            r2 = (r * a) / 255;
            g2 = (g * a) / 255;
            b2 = (b * a) / 255;
            pBmpPixels[y * cxBmp + x] = argb(a, r2, g2, b2);
        }
    }
    SetBkMode(hdcBmp, TRANSPARENT);
    SetTextColor(hdcBmp, RGB(255, 255, 0));
    hFont = CreateFont(-MulDiv(14, 96, 72), 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, TEXT("Times New Roman"));
    old_hFont = SelectObject(hdcBmp, hFont);
    for (x = 1; x < 10; ++x)
    {
        TCHAR sz[99];
        wsprintf(sz, TEXT("Where do you want to go today? Station %d?"), x);
        My32bppTextOut(hdcBmp, pBmpPixels, cxBmp, cyBmp, 20, x * 28, sz, -1);
    }
    SelectObject(hdcBmp, old_hFont);
    DeleteObject(hFont);
    return TRUE;
}

static LRESULT CALLBACK MyWndProc(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
    LRESULT lResult = 0;
    BOOL fOverrid = 0;
    if (msg == WM_SIZE)
    {
        if (!IsIconic(hWnd))
        {
            Boon_FullUpdateLayeredWindow32bpp(hWnd, MyRenderProc, NULL);
        }
    }
    else if (msg == WM_DESTROY)
    {
        PostQuitMessage(0);
    }
    else if (msg == WM_KEYDOWN)
    {
        WORD vk = LOWORD(wParam);
        if (vk == VK_RETURN)
        {
            static BOOL doEnableBlueBehind = TRUE;
            BOOL ok = Boon_EnableBlurBehind(hWnd, doEnableBlueBehind);
            TCHAR sz[99];
            wsprintf(sz, TEXT("AccentBlurBehind %s"), !ok ? "Error" :
                (doEnableBlueBehind ? "Enabled" : "Disabled")
            );
            MessageBoxA(hWnd, sz, MyWndClassName, ok ? MB_ICONASTERISK : MB_ICONHAND);
            if (ok) { doEnableBlueBehind = !doEnableBlueBehind; }
        }
    }
    if (!fOverrid)
    {
        lResult = DefWindowProc(hWnd, msg, wParam, lParam);
    }
    if (msg == WM_NCHITTEST)
    {
        if (lResult == HTCLIENT)
        {
            lResult = HTCAPTION;
        }
    }
    return lResult;
}

int AppMain(void)
{
    WNDCLASS wc = { 0 };
    HWND hWnd = 0;
    MSG msg = { 0 };
    wc.hCursor = LoadCursor(0, IDC_ARROW);
    wc.lpszClassName = MyWndClassName;
    wc.lpfnWndProc = MyWndProc;
    RegisterClass(&wc);
    hWnd = CreateWindowEx(
        WS_EX_LAYERED,
        MyWndClassName, MyWndClassName,
        WS_POPUP | WS_SYSMENU | WS_SIZEBOX | WS_MINIMIZEBOX | WS_MAXIMIZEBOX,
        // x, y, width, height
        50, 50, 500, 400,
        // hWndParent, hMenu, hInst, lpParam
        0, 0, 0, 0);
    ShowWindow(hWnd, SHOW_OPENWINDOW);
    while (GetMessage(&msg, 0, 0, 0))
    {
        TranslateMessage(&msg);
        DispatchMessage(&msg);
    }
    return (int)(msg.wParam);
}

void RawMain(void)
{
	ExitProcess(AppMain());
}
