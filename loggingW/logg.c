#include "logg.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

static DWORD appendLogFile(WCHAR const *pszLine)
{
	DWORD winerr = 0;
	HANDLE hFile = 0;
	ULARGE_INTEGER filesize;
	DWORD cbWri = 0;
	hFile = CreateFileW(L"C:\\q\\boon.log"
		, FILE_APPEND_DATA
		, FILE_SHARE_READ, 0
		, OPEN_ALWAYS, FILE_FLAG_WRITE_THROUGH, 0);
	if (hFile == INVALID_HANDLE_VALUE) {
		hFile = 0;
		winerr = GetLastError();
		goto eof;
	}
	ZeroMemory(&filesize, sizeof(filesize));
	filesize.LowPart = GetFileSize(hFile, &filesize.HighPart);
	if (filesize.QuadPart == 0) {
		if (!WriteFile(hFile, "\xFF\xFE", 2, &cbWri, NULL)) {
			winerr = GetLastError();
			goto eof;
		}
	}
	if (!WriteFile(hFile, pszLine, lstrlenW(pszLine) * sizeof(WCHAR), &cbWri, 0)) {
		winerr = GetLastError();
		goto eof;
	}
	if (!WriteFile(hFile, L"\r\n", 4, &cbWri, NULL)) {
		winerr = GetLastError();
		goto eof;
	}
eof:
	if (hFile) CloseHandle(hFile);
	return winerr;
}

EXTERN_C int CDECL logg(WCHAR const *pszFmt, ...)
{
	WCHAR szLine[4096], *p = szLine;
	SYSTEMTIME st;
	va_list ap;
	GetLocalTime(&st);
	p += wsprintfW(p, L"[%.4u-%.2u-%.2u %.2u:%.2u:%.2u.%.3u][%5lu][%5lu]: ",
		st.wYear, st.wMonth, st.wDay, st.wHour,
		st.wMinute, st.wSecond, st.wMilliseconds,
		GetCurrentProcessId(), GetCurrentThreadId());
	va_start(ap, pszFmt);
	p += wvsprintfW(p, pszFmt, ap);
	va_end(ap);
	appendLogFile(szLine);
	return 0;
}
