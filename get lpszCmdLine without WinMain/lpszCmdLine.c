#include <Windows.h>

static LPCTSTR MyGetCommandLine(void)
{
    LPCTSTR lp = GetCommandLine();
	/* skip first double-quoted string */
    if (*lp == '\"')
    {
        for (++lp; *lp; ++lp)
		{
			if (*lp == '\"') { ++lp; break; }
		}
    }
	/* loop to space char */
	for (; *lp && *lp != ' '; ++lp);
	/* loop to non-space char */
    for (; *lp == ' '; ++lp);
    return lp;
}

int main(void)
{
    LPCTSTR lpszCmdLine = MyGetCommandLine();
    MessageBox(0, lpszCmdLine, TEXT("lpszCmdLine"), 0);
    return 0;
}
