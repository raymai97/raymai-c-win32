#include <Windows.h>
#include "get_ntoskrnl_ver.h"

void RawMain(void)
{
	struct ntoskrnl_ver ver;
	int err = get_ntoskrnl_ver(&ver);
	TCHAR szMsg[100];
	TCHAR const *pszTitle = TEXT("ntoskrnl version");
	if (err)
	{
		wsprintf(szMsg, TEXT("Error %d"), err);
		MessageBox(0, szMsg, pszTitle, MB_ICONHAND);
	}
	else
	{
		wsprintf(szMsg,
			TEXT("Major: %u \n")
			TEXT("Minor: %u \n")
			TEXT("Build: %u \n")
			TEXT("Revision: %u"),
			ver.major, ver.minor, ver.build, ver.revision);
		MessageBox(0, szMsg, pszTitle, MB_ICONASTERISK);
	}
	ExitProcess(0);
}
