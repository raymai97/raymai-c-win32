Build the program with MSVC97 to get tiny EXE (2KB).

Tested OK on:
Windows XP Pro SP3 (32-bit)
Windows Vista Ent SP2 (64-bit)
Windows 7 Ent SP1 (64-bit)
Windows 8 Pro (32-bit)
Windows 8.1 Pro VL UP3 (32-bit)
Windows 10 ProWks 21H1 (64-bit)

Tested error on:
Windows ME
