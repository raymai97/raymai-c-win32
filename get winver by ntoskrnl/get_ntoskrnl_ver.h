#pragma once
#include <Windows.h>

struct ntoskrnl_ver {
	WORD major;
	WORD minor;
	WORD build;
	WORD revision;
};

int get_ntoskrnl_ver(struct ntoskrnl_ver *pVer);
