#include "get_ntoskrnl_ver.h"
#include <winver.h>

int get_ntoskrnl_ver(struct ntoskrnl_ver *pVer)
{
	TCHAR szPath[MAX_PATH];
	DWORD fviHandle = 0;
	DWORD fviLen = 0;
	BYTE fviData[10240];
	VS_FIXEDFILEINFO *pFFI = 0;
	UINT cbFFI = 0;
	memset(pVer, 0, sizeof(*pVer));
	GetSystemDirectory(szPath, MAX_PATH);
	lstrcat(szPath, TEXT("\\ntoskrnl.exe"));
	fviLen = GetFileVersionInfoSize(szPath, &fviHandle);
	if (fviLen < 1 || fviLen > sizeof(fviData))
	{
		return -1;
	}
	if (!GetFileVersionInfo(szPath, fviHandle, fviLen, fviData))
	{
		return -2;
	}
	if (!VerQueryValue(fviData, TEXT("\\"), (void**)&pFFI, &cbFFI))
	{
		return -3;
	}
	pVer->major = HIWORD(pFFI->dwProductVersionMS);
	pVer->minor = LOWORD(pFFI->dwProductVersionMS);
	pVer->build = HIWORD(pFFI->dwProductVersionLS);
	pVer->revision = LOWORD(pFFI->dwProductVersionLS);
	return 0;
}
