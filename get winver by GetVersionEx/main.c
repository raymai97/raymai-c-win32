#include <Windows.h>

void RawMain(void)
{
	OSVERSIONINFO vi = { sizeof(vi) };
	TCHAR szMsg[512];
	if (GetVersionEx(&vi))
	{
		switch (vi.dwPlatformId) {
		case 1: // Win9x returns Major and Minor in HIWORD, we don't want that
			vi.dwBuildNumber = LOWORD(vi.dwBuildNumber);
			break;
		}
		wsprintf(szMsg,
			TEXT("PlatformID: %u\n")
			TEXT("Major: %u \n")
			TEXT("Minor: %u \n")
			TEXT("Build: %u \n")
			TEXT("CSD: %s"),
			vi.dwPlatformId,
			vi.dwMajorVersion,
			vi.dwMinorVersion,
			vi.dwBuildNumber,
			vi.szCSDVersion
		);
		MessageBox(0, szMsg, TEXT("GetVersionEx"), MB_ICONASTERISK);
	}
	else
	{
		wsprintf(szMsg, TEXT("Win32 error %u"), GetLastError());
		MessageBox(0, szMsg, TEXT("GetVersionEx"), MB_ICONASTERISK);
	}
	ExitProcess(0);
}
