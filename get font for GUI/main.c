#include <Windows.h>

#ifndef GetWindowLongPtr
#define GetWindowLongPtr  GetWindowLong
#endif
#ifndef SetWindowLongPtr
#define SetWindowLongPtr  SetWindowLong
#endif

#define SAFEFREE(p,fn)  if (p) { fn(p); (p) = 0; }

#define MainWndClassName  TEXT("GUIFont.MainWnd")
#define MainWndTitle  TEXT("GUIFont")

typedef struct MainWndSelf {
    HFONT hfoMessageFont;
    HWND hButton1;
    HWND hButton2;
    HWND hButton3;
} MainWndSelf;

BOOL InitMainWndSelf(MainWndSelf *pSelf)
{
    NONCLIENTMETRICS ncm = { sizeof(ncm) };
    SystemParametersInfo(SPI_GETNONCLIENTMETRICS, sizeof(NONCLIENTMETRICS), &ncm, 0);
    pSelf->hfoMessageFont = CreateFontIndirect(&ncm.lfMessageFont);
    return TRUE;
}

void UninitMainWndSelf(MainWndSelf *pSelf)
{
    SAFEFREE(pSelf->hfoMessageFont, DeleteObject);
}

LRESULT CALLBACK MainWndProc(HWND hWnd, UINT msg, WPARAM w, LPARAM l)
{
    LRESULT lRes = 0;
    BOOL fOverrid = FALSE;
    MainWndSelf *pSelf = 0;
    if (msg == WM_NCCREATE)
    {
        pSelf = HeapAlloc(GetProcessHeap(), HEAP_ZERO_MEMORY, sizeof(*pSelf));
        SetWindowLongPtr(hWnd, 0, (LPARAM)pSelf);
        if (!pSelf || !InitMainWndSelf(pSelf))
        {
            DestroyWindow(hWnd);
            return FALSE;
        }
    }
    else
    {
        pSelf = (MainWndSelf*)GetWindowLongPtr(hWnd, 0);
    }
    if (msg == WM_NCDESTROY)
    {
        if (pSelf) UninitMainWndSelf(pSelf);
    }
    if (msg == WM_CREATE)
    {
        LPCTSTR text1 = TEXT("2021: Where do you want to go today?");
#ifdef UNICODE
        LPCTSTR text2 = L"2021\x306E\x4ECA\x65E5\x306F\x3069\x3053\x306B\x3044\x304F\x306E";
#else
        LPCTSTR text2 = "2021\x82\xCC\x8D\xA1\x93\xFA\x82\xCD\x82\xC7\x82\xB1\x82\xC9\x82\xA2\x82\xAD\x82\xCC";
#endif
#ifdef UNICODE
        LPCTSTR text3 = L"2021\x5E74\x7684\x4ECA\x5929\x8981\x53BB\x54EA\x513F\x5462";
#else
        LPCTSTR text3 = "2021\xC4\xEA\xB5\xC4\xBD\xF1\xCC\xEC\xD2\xAA\xC8\xA5\xC4\xC4\xB6\xF9\xC4\xD8";
#endif
        int y = 10;
        pSelf->hButton1 = CreateWindow(TEXT("BUTTON")
            , text1
            , WS_CHILD | WS_VISIBLE | WS_TABSTOP
            , 10, y, 300, 28
            , hWnd, 0, 0, 0);
        y += 28 + 3;
        pSelf->hButton2 = CreateWindow(TEXT("BUTTON")
            , text2
            , WS_CHILD | WS_VISIBLE | WS_TABSTOP
            , 10, y, 300, 28
            , hWnd, 0, 0, 0);
        y += 28 + 3;
        pSelf->hButton3 = CreateWindow(TEXT("BUTTON")
            , text3
            , WS_CHILD | WS_VISIBLE | WS_TABSTOP
            , 10, y, 300, 28
            , hWnd, 0, 0, 0);
        y += 28 + 3;
        SendMessage(pSelf->hButton1, WM_SETFONT, (WPARAM)(pSelf->hfoMessageFont), 0);
        SendMessage(pSelf->hButton2, WM_SETFONT, (WPARAM)(pSelf->hfoMessageFont), 0);
        SendMessage(pSelf->hButton3, WM_SETFONT, (WPARAM)(pSelf->hfoMessageFont), 0);
    }
    if (!fOverrid)
    {
        lRes = DefWindowProc(hWnd, msg, w, l);
    }
    return lRes;
}

void RawMain(void)
{
    WNDCLASS wc = { 0 };
    HWND hWnd = 0;
    wc.cbWndExtra = sizeof(LPARAM);
    wc.hbrBackground = (HBRUSH)(COLOR_WINDOW + 1);
    wc.hCursor = LoadCursor(0, IDC_ARROW);
    wc.lpszClassName = MainWndClassName;
    wc.lpfnWndProc = MainWndProc;
    wc.style = CS_HREDRAW | CS_VREDRAW;
    RegisterClass(&wc);
    hWnd = CreateWindow(MainWndClassName, MainWndTitle
        , WS_OVERLAPPEDWINDOW
        , CW_USEDEFAULT, 0, CW_USEDEFAULT, 0
        , 0, 0, 0, 0);
    if (hWnd)
    {
        ShowWindow(hWnd, SW_NORMAL);
        while (IsWindow(hWnd))
        {
            MSG msg;
            if (GetMessage(&msg, 0, 0, 0))
            {
                if (IsDialogMessage(hWnd, &msg)) continue;
                TranslateMessage(&msg);
                DispatchMessage(&msg);
            }
        }
    }
    ExitProcess(0);
}
