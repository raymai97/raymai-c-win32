@echo off
call :cleanup

REM Set CFLAGS to "-DUNICODE" for UNICODE build.
cl -c -MD %CFLAGS% ^
    main.c || goto gg

link ^
    kernel32.lib ^
    user32.lib ^
    gdi32.lib ^
    main.obj ^
    -subsystem:windows ^
    -entry:RawMain ^
    -fixed:no || goto gg

call :cleanup
exit/b

:gg
echo.
echo. ERROR
echo.
pause
exit/b

:cleanup
if exist main.obj del main.obj
exit/b
